package com.pitang.anderson.duarte.dto;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import com.pitang.anderson.duarte.model.Car;
import com.pitang.anderson.duarte.model.User;
import com.pitang.anderson.duarte.utils.ApplicationUtils;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;



/**
 * Hash code.
 *
 * @return the int
 */

/**
 * Hash code.
 *
 * @return the int
 */
@EqualsAndHashCode(callSuper=false)
public class UserCreateDTO extends UserDTO {

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	
	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	@Getter
	
	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	
	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	@Setter
	private String password;

	/**
	 * Convert DTO to user.
	 *
	 * @param dto the dto
	 * @return the user
	 */
	public static User convertDTOToUser(UserCreateDTO dto) {
		Mapper mapper = DozerBeanMapperBuilder.buildDefault();
		User user = mapper.map(dto, User.class);
		user.setCars(ApplicationUtils.mapBeans(dto.getCars(), Car.class));
		return user;
	}
}
