package com.pitang.anderson.duarte.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.pitang.anderson.duarte.enums.TypeUploadEnum;
import com.pitang.anderson.duarte.exception.DataAlreadyFoundException;
import com.pitang.anderson.duarte.exception.DataNotFoundException;
import com.pitang.anderson.duarte.exception.ErrorCode;
import com.pitang.anderson.duarte.exception.InvalidFieldsException;
import com.pitang.anderson.duarte.exception.MissingFieldsException;
import com.pitang.anderson.duarte.exception.UploadException;
import com.pitang.anderson.duarte.model.Car;
import com.pitang.anderson.duarte.model.User;
import com.pitang.anderson.duarte.repository.CarRepository;
import com.pitang.anderson.duarte.repository.UserRepository;
import com.pitang.anderson.duarte.util.PitangValidator;


/**
 * The Class CarServiceImpl.
 */
@Service
@Transactional
public class CarServiceImpl implements CarService {

	/** The car repository. */
	private CarRepository carRepository;
	
	/** The user repository. */
	private UserRepository userRepository;
	
	/** The upload service. */
	private UploadService uploadService;

	/**
	 * Instantiates a new car service impl.
	 *
	 * @param carRepository the car repository
	 * @param userRepository the user repository
	 * @param uploadService the upload service
	 */
	@Autowired
	public CarServiceImpl(CarRepository carRepository, UserRepository userRepository, UploadService uploadService) {
		this.carRepository = carRepository;
		this.userRepository = userRepository;
		this.uploadService = uploadService;
	}

	/**
	 * Gets the all cars by user.
	 *
	 * @param login the login
	 * @return the all cars by user
	 */
	@Override
	public List<Car> getAllCarsByUser(String login) {
		User user = findUserByLogin(login);
		return carRepository.findByUser(user);
	}

	/**
	 * Save car by user.
	 *
	 * @param login the login
	 * @param car the car
	 * @return the car
	 */
	@Override
	public Car saveCarByUser(String login, Car car) {
		checkMissingFilds(car);
		checkInvalidFields(car);
		User user = findUserByLogin(login);
		checkExistLicensePlate(car);
		car.setUser(user);
		return carRepository.save(car);
	}

	/**
	 * Find by car id and user.
	 *
	 * @param id the id
	 * @param login the login
	 * @return the car
	 */
	@Override
	public Car findByCarIdAndUser(Integer id, String login) {
		User user = findUserByLogin(login);
		return findCarByIdAndUser(id, user);
	}

	/**
	 * Update car by id and user.
	 *
	 * @param id the id
	 * @param newCar the new car
	 * @param login the login
	 * @return the car
	 */
	@Override
	public Car updateCarByIdAndUser(Integer id, Car newCar, String login) {
		checkMissingFilds(newCar);
		checkInvalidFields(newCar);
		User user = findUserByLogin(login);
		Car car = findCarByIdAndUser(id, user);
		car.setLicensePlate(newCar.getLicensePlate());
		car.setColor(newCar.getColor());
		car.setYear(car.getYear());
		car.setModel(newCar.getModel());
		return carRepository.save(car);
	}

	/**
	 * Delete car by id and user.
	 *
	 * @param id the id
	 * @param login the login
	 */
	@Override
	public void deleteCarByIdAndUser(Integer id, String login) {
		User user = findUserByLogin(login);
		user.getCars().removeIf(c -> c.getCarId().equals(id));
		Car car = findCarByIdAndUser(id, user);
		carRepository.delete(car);
	}

	/**
	 * Find by license plate.
	 *
	 * @param licensePlate the license plate
	 * @return the car
	 */
	@Override
	public Car findByLicensePlate(String licensePlate) {
		return carRepository.findByLicensePlate(licensePlate);
	}

	/**
	 * Upload car image.
	 *
	 * @param file the file
	 * @param id the id
	 * @param login the login
	 * @param uploadEnum the upload enum
	 * @return the car
	 */
	@Override
	public Car uploadCarImage(MultipartFile file, Integer id, String login, TypeUploadEnum uploadEnum) {
		User user = findUserByLogin(login);
		Car car = findCarByIdAndUser(id, user);
		try {
			String uploadPhoto = uploadService.uploadPhoto(file, login, uploadEnum);
			car.setPhotoDirectory(uploadPhoto);
			return carRepository.saveAndFlush(car);
		} catch (Exception e) {
			throw new UploadException(ErrorCode.ERROR_UPLOAD_IMAGE);
		}
	}

	/**
	 * Check exist license plate.
	 *
	 * @param car the car
	 */
	private void checkExistLicensePlate(Car car) {
		if (findByLicensePlate(car.getLicensePlate()) != null) {
			throw new DataAlreadyFoundException(ErrorCode.LICENSE_PLATE_ALREADY_FOUND);
		}
	}

	/**
	 * Find user by login.
	 *
	 * @param login the login
	 * @return the user
	 */
	private User findUserByLogin(String login) {
		User user = userRepository.findByLogin(login);
		if (user == null) {
			throw new DataNotFoundException(ErrorCode.USER_NOT_FOUND);
		}
		return user;
	}

	/**
	 * Find car by id and user.
	 *
	 * @param id the id
	 * @param user the user
	 * @return the car
	 */
	private Car findCarByIdAndUser(Integer id, User user) {
		Car car = carRepository.findByCarIdAndUser(id, user);
		if (car == null) {
			throw new DataNotFoundException(ErrorCode.CAR_NOT_FOUND);
		}
		return car;
	}

	/**
	 * Check missing fields list.
	 *
	 * @param cars the cars
	 */
	public static void checkMissingFieldsList(List<Car> cars) {
		for (Car car : cars) {
			checkMissingFilds(car);
		}
	}

	/**
	 * Check missing filds.
	 *
	 * @param car the car
	 */
	private static void checkMissingFilds(Car car) {
		if (StringUtils.isEmpty(car.getColor()) || StringUtils.isEmpty(car.getLicensePlate())
				|| StringUtils.isEmpty(car.getModel())) {
			throw new MissingFieldsException();
		}
	}

	/**
	 * Check invalid fields list.
	 *
	 * @param cars the cars
	 */
	public static void checkInvalidFieldsList(List<Car> cars) {
		for (Car car : cars) {
			checkInvalidFields(car);
		}
	}

	/**
	 * Check invalid fields.
	 *
	 * @param car the car
	 */
	private static void checkInvalidFields(Car car) {
		if (!PitangValidator.licensePlateIsValid(car.getLicensePlate()) || car.getYear() < 1) {
			throw new InvalidFieldsException();
		}
	}
}
