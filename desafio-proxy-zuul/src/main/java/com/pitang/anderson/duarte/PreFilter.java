package com.pitang.anderson.duarte;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

import lombok.extern.slf4j.Slf4j;


/**
 * The Class PreFilter.
 */
@Component

/** The Constant log. */
@Slf4j
public class PreFilter extends ZuulFilter {

	/**
	 * Filter type.
	 *
	 * @return the string
	 */
	@Override
	public String filterType() {
		return "pre";
	}

	/**
	 * Filter order.
	 *
	 * @return the int
	 */
	@Override
	public int filterOrder() {
		return 6;
	}

	/**
	 * Should filter.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean shouldFilter() {
		return true;
	}

	/**
	 * Run.
	 *
	 * @return the object
	 */
	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		ctx.addZuulRequestHeader("Authorization", request.getHeader("Authorization"));

		log.info("Parametres : {}",
				request.getParameterMap().entrySet().stream()
						.map(e -> e.getKey() + "=" + Stream.of(e.getValue()).collect(Collectors.toList()))
						.collect(Collectors.toList()));
		log.info("Headers : {}", "Authorization" + "=" + request.getHeader("Authorization"));
		log.info(String.format("%s request to %s", request.getMethod(), request.getRequestURL().toString()));
		return null;
	}
}
