package com.pitang.anderson.duarte.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.pitang.anderson.duarte.model.User;
import com.pitang.anderson.duarte.repository.UserRepository;


/**
 * The Class UserDetailsServiceImpl.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	/** The user repository. */
	private UserRepository userRepository;

	/**
	 * Instantiates a new user details service impl.
	 *
	 * @param userRepository the user repository
	 */
	@Autowired
	public UserDetailsServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	/**
	 * Load user by username.
	 *
	 * @param login the login
	 * @return the user details
	 */
	@Override
	public UserDetails loadUserByUsername(String login) {
		User user = userRepository.findByLogin(login);
		if (user == null) {
			throw new UsernameNotFoundException("Username not found.");
		}
		return user;
	}
}
