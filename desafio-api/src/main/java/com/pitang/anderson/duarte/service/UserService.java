package com.pitang.anderson.duarte.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.pitang.anderson.duarte.enums.TypeUploadEnum;
import com.pitang.anderson.duarte.model.User;


/**
 * The Interface UserService.
 */
public interface UserService {

	/**
	 * Gets the all users.
	 *
	 * @return the all users
	 */
	public List<User> getAllUsers();

	/**
	 * Save user.
	 *
	 * @param user the user
	 * @return the user
	 */
	public User saveUser(User user);

	/**
	 * Find user by id.
	 *
	 * @param id the id
	 * @return the user
	 */
	public User findUserById(Integer id);

	/**
	 * Update user.
	 *
	 * @param id the id
	 * @param user the user
	 * @return the user
	 */
	public User updateUser(Integer id, User user);

	/**
	 * Delete user.
	 *
	 * @param id the id
	 */
	public void deleteUser(Integer id);

	/**
	 * Find by login.
	 *
	 * @param login the login
	 * @return the user
	 */
	public User findByLogin(String login);

	/**
	 * Save last login.
	 *
	 * @param login the login
	 */
	public void saveLastLogin(String login);

	/**
	 * Upload user image.
	 *
	 * @param file the file
	 * @param name the name
	 * @param user the user
	 * @return the user
	 */
	public User uploadUserImage(MultipartFile file, String name, TypeUploadEnum user);
	
}
