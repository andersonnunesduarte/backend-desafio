package com.pitang.anderson.duarte.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.ws.rs.core.Application;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.pitang.anderson.duarte.controller.UserController;
import com.pitang.anderson.duarte.dto.UserCreateDTO;
import com.pitang.anderson.duarte.exception.DataNotFoundException;


/**
 * The Class UserTest.
 */
public class UserTest {
	
	/**
	 * The Class TestsUser.
	 */
	@SpringBootTest(classes = Application.class)
	@ActiveProfiles("test")
	@RunWith(SpringRunner.class)
	public class TestsUser {

		/** The user controller. */
		@Autowired
		private UserController userController;
		
		/**
		 * Test get all user.
		 */
		@Test
		public void testGetAllUser() {
			try {
				create();
				assertTrue(userController.getAllUsers() != null);
			} catch (EmptyResultDataAccessException e) {
				assertTrue(e != null);
			} catch (DataNotFoundException e) {
				assertTrue(e != null);
			} catch (Exception e) {
				assertFalse(e != null);
			}
		}
		
		/**
		 * Creates the.
		 */
		@Test
		public void create() {
			try {
				UserCreateDTO user = new UserCreateDTO();
				user.setUserId(1L);
				user.setFirstName("Anderson");
				user.setLastName("Duarte");
				user.setBirthday(new Date());
				user.setEmail("anderson.nunes.duarte@gmail.com");
				user.setLogin("anderson.duarte");
				user.setPhone("11986503235");
				user.setPassword("password");
				assertTrue(userController.createUser(user) != null);
			} catch (EmptyResultDataAccessException e) {
				assertTrue(e != null);
			} catch (DataNotFoundException e) {
				assertTrue(e != null);
			} catch (Exception e) {
				assertFalse(e != null);
			}
		}
	}
}
