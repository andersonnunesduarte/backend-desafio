# API RESTful Desafio Pitang 

## User Stories 

### 1 - Usuário acessa a tela de login do sistema
#### Critérios:
– Usuário acessa o site com login e senha corretos.

– Usuário visualiza mensagem de erro quando tenta acessar com login incorreto.

– Usuário visualiza mensagem de erro quando ele não confirmou sua conta.

– Usuário visualiza mensagem de erro quando conta não existe.

### 2 - Usuário acessa a tela de registro do sistema
#### Critérios:
– Usuário preenche informações com todos dados corretos.

– Usuário visualiza mensagem de erro quando tenta cadastrar com login ja existente.

– Usuário visualiza mensagem de erro quando tenta cadastrar com email ja existente.

– Usuário visualiza mensagem de erro quando ele não confirmou todos os dados

– Usuário visualiza mensagem de erro quando ele não insere um email valido.

– Usuário visualiza mensagem de erro quando ele não insere um telefone valido.

### 3 - Usuário acessa a tela de informações do usuario.
#### Critérios:
– Usuário preenche informações com todos dados corretos.

– Usuário visualiza mensagem de erro quando ele não confirmou todos os dados

– Usuário visualiza mensagem de erro quando ele não insere um email valido.

– Usuário visualiza mensagem de erro quando ele não insere um telefone valido.

### 4 - Usuário acessa a tela de lista de carros
#### Critérios:
– Usuário visualiza carros cadastrados.

### 5 - Usuário acessa a tela de cadastro de carros
#### Critérios:
– Usuário preenche informações com todos dados corretos.

– Usuário visualiza mensagem de erro quando ele não confirmou todos os dados

– Usuário visualiza mensagem de erro quando ele não insere uma placa válida

– Usuário visualiza mensagem de erro quando ele não insere uma placa já existente.

## Solução
Para o rápido desenvolvimento da API foi utilizado diversas ferramentas como o springboot, que facilita e torna o processo de desenvolvimento mais ágil, assim como frameworks que deixam o projeto mais limpo e claro facilitando a leitura futuramente. Foi implementado também o Eureka para controle de serviços, assim como o proxy Zuul deixando assim a aplicação mais restrita, disponibilizando somente a porta :8080 para acesso a todos serviços.
  
## Build
Comando para build do projeto:
```sh
$ mvn clean install
```

## Deploy
Após clone do projeto realizar o comando abaixo para os modulos: desafio-api, desafio-eureka e desafio-proxy-zuul.

```sh
$ mvn spring-boot:run
```

## Testes
Comando para testes do projeto:
```sh
$ mvn test
```