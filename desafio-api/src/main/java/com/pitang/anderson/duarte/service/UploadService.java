package com.pitang.anderson.duarte.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.pitang.anderson.duarte.enums.TypeUploadEnum;
import com.pitang.anderson.duarte.exception.ErrorCode;
import com.pitang.anderson.duarte.exception.UploadException;

import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;


/**
 * The Class UploadService.
 */
@Service

/** The Constant log. */

/** The Constant log. */
@Slf4j
public class UploadService {

	/** The directory photo car. */
	@Value("${directory.photo.car}")
	private String directoryPhotoCar;

	/** The directory photo user. */
	@Value("${directory.photo.user}")
	private String directoryPhotoUser;

	/**
	 * Upload photo.
	 *
	 * @param photo the photo
	 * @param login the login
	 * @param uploadEnum the upload enum
	 * @return the string
	 */
	public String uploadPhoto(MultipartFile photo, String login, TypeUploadEnum uploadEnum) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String pathPhoto = TypeUploadEnum.CAR.equals(uploadEnum) ? directoryPhotoCar : directoryPhotoUser;
		Path diretorioPath = Paths.get(System.getProperty("user.home") + pathPhoto);
		Path arquivoPath = diretorioPath.resolve(login + sdf.format(new Date()) + ".jpg");
		try {
			Files.createDirectories(diretorioPath);
			photo.transferTo(arquivoPath.toFile());
			resizeImage(arquivoPath.toFile());
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			throw new UploadException(ErrorCode.ERROR_UPLOAD_IMAGE);
		}
		return arquivoPath.toAbsolutePath().toString();
	}
	
	/**
	 * Resize image.
	 *
	 * @param file the file
	 */
	private void resizeImage(File file) {
		try {
			Thumbnails.of(file)
			.size(640, 480)
			.outputFormat("jpg")
			.toFiles(Rename.NO_CHANGE);
		} catch (IOException e) {
			log.error(ErrorCode.ERROR_UPLOAD_CONVERT_IMAGE.getMessage());
		}
	}

}
