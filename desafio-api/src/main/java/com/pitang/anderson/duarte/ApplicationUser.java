package com.pitang.anderson.duarte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * The Class ApplicationUser.
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.pitang.anderson.duarte.controller, com.pitang.anderson.duarte.service, com.pitang.anderson.duarte.security")
@EnableJpaRepositories("com.pitang.anderson.duarte.repository")
@EntityScan("com.pitang.anderson.duarte.model")
@EnableSwagger2
@EnableDiscoveryClient
public class ApplicationUser {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(ApplicationUser.class, args);
	}

	/**
	 * B crypt password encoder.
	 *
	 * @return the b crypt password encoder
	 */
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
