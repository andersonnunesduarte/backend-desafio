package com.pitang.anderson.duarte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;


/**
 * The Class ZuuApplication.
 */
@EnableZuulProxy
@EnableEurekaClient
@SpringBootApplication
public class ZuuApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(ZuuApplication.class, args);
	}

}
