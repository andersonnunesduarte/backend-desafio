package com.pitang.anderson.duarte.exception;

/**
 * The Class DataAlreadyFoundException.
 */
public class DataAlreadyFoundException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The code. */
	private final int code;

	/**
	 * Instantiates a new data already found exception.
	 *
	 * @param msg the msg
	 * @param code the code
	 */
	public DataAlreadyFoundException(String msg, int code) {
		super(msg);
		this.code = code;
	}

	/**
	 * Instantiates a new data already found exception.
	 *
	 * @param msg the msg
	 * @param cause the cause
	 * @param code the code
	 */
	public DataAlreadyFoundException(String msg, Throwable cause, int code) {
		super(msg, cause);
		this.code = code;
	}
	
	/**
	 * Instantiates a new data already found exception.
	 *
	 * @param errorCode the error code
	 */
	public DataAlreadyFoundException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
    }
	
	/**
	 * Instantiates a new data already found exception.
	 *
	 * @param msg the msg
	 * @param errorCode the error code
	 */
	public DataAlreadyFoundException(String msg, ErrorCode errorCode) {
	    super(msg);
	    this.code = errorCode.getCode();
	}
	
	/**
	 * Instantiates a new data already found exception.
	 *
	 * @param msg the msg
	 * @param cause the cause
	 * @param errorCode the error code
	 */
	public DataAlreadyFoundException(String msg, Throwable cause, ErrorCode errorCode) {
        super(msg, cause);
        this.code = errorCode.getCode();
    }
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public int getCode() {
        return code;
    }

}
