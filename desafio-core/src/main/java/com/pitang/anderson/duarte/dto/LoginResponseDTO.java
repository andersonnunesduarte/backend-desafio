package com.pitang.anderson.duarte.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;



/**
 * Instantiates a new login response DTO.
 */

/**
 * Instantiates a new login response DTO.
 *
 * @param token the token
 */
@AllArgsConstructor
public class LoginResponseDTO  {

	/**
	 * Gets the jwttoken.
	 *
	 * @return the jwttoken
	 */
	
	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	@Getter
	private String token;

}
